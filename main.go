package main

import (
	"fmt"

	"gitlab.com/xccelerator/golang-online-shop/authentication/api"
	"gitlab.com/xccelerator/golang-online-shop/authentication/api/database"
)

func main() {
	database.ConnectDatabase()
	fmt.Println("Database was started successfully!")

	srv := api.CreateServer(":40501")
	fmt.Println("Server is running on PORT: 40501...")

	srv.ListenAndServe()
}
