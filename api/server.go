package api

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/xccelerator/golang-online-shop/authentication/api/router"
)

type Server struct {
	*mux.Router
}

func CreateServer(port string) *http.Server {
	router := router.CreateRouter()

	router.Resource("/authentication").AuthEndpoints()
	router.Resource("/user").UserEndpoints()

	s := &http.Server{
		Addr:    port,
		Handler: router,
	}

	return s
}
