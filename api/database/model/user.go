package model

import "github.com/google/uuid"

type User struct {
	Id            uuid.UUID
	email         string
	username      string
	password_hash string
}
