package database

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq" // Postgres goalng driver
)

const (
	host     = "localhost"
	port     = 4000
	user     = "postgres"
	password = "postgres"
	dbName   = "postgres"
)

var Instance *sql.DB
var err error

func ConnectDatabase() {
	sqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbName)

	Instance, err = sql.Open("postgres", sqlInfo)
	if err != nil {
		panic(err)
	}

	err = Instance.Ping()
	if err != nil {
		panic(err)
	}
}
