CREATE TABLE IF NOT EXISTS "db_user" (
    user_id UUID PRIMARY KEY,
    email varchar(255) NOT NULL,
    username varchar(255) NOT NULL UNIQUE,
    password_hash varchar(255) NOT NULL
)