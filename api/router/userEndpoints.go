package router

import "gitlab.com/xccelerator/golang-online-shop/authentication/api/controller"

func (r *Router) UserEndpoints() {
	// Get all users
	// PATH:
	// ARGUMENTS:
	// RETURN:
	// DESCRIPTION:
	r.Endpoint("POST", "/", controller.GetUsers)

	// Get user by id
	r.Endpoint("POST", "/{id}", controller.GetUserById)

	// Delete user
	r.Endpoint("DELETE", "/{id}", controller.DeleteUser)

	// Update user
	r.Endpoint("PUT", "/{id}", controller.UpdateUser)
}
