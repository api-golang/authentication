package router

import "net/http"

type Route struct {
	Path    string
	Method  string
	Handler http.HandlerFunc
}

type Router struct {
	Mux    *http.ServeMux
	Routes []Route
	Prefix string
}

func (r *Router) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	for _, rt := range r.Routes {
		if req.Method != rt.Method {
			continue
		}

		if req.URL.Path != rt.Path {
			continue
		}

		rt.Handler.ServeHTTP(w, req)
		return
	}

	http.NotFound(w, req)
}

func CreateRouter() *Router {
	return &Router{
		Mux:    http.NewServeMux(),
		Routes: []Route{},
	}
}

func (r *Router) Resource(prefix string) *Router {
	r.Mux.Handle(prefix+"/", http.StripPrefix(prefix, r.Mux))
	r.Prefix += prefix
	return r
}

func (r *Router) Endpoint(method, path string, handler http.HandlerFunc) {
	r.Routes = append(r.Routes, Route{Path: r.Prefix + path, Method: method, Handler: handler})
}
