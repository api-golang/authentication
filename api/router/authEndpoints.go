package router

import "gitlab.com/xccelerator/golang-online-shop/authentication/api/controller"

func (r *Router) AuthEndpoints() {
	// Sign Up
	// Registration
	r.Endpoint("GET", "/sign-up", controller.CreateUser)

	// Sign In
	r.Endpoint("POST", "/sign-in", controller.SignIn)
}
