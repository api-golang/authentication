module gitlab.com/xccelerator/golang-online-shop/authentication

go 1.21.1

require (
	github.com/google/uuid v1.3.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/lib/pq v1.10.9 // indirect
)
